# Round 1

## Test environments

* Windows 7, R 4.1.1  (local)

## Submission comments

2021-10-28

## R CMD check results

0 ERRORs | 0 WARNINGs | 0 NOTEs





# Round 1

## Test environments

* Windows 7, R 3.6.2  (local)

## Submission comments

2020-01-23

## R CMD check results

0 ERRORs | 0 WARNINGs | 0 NOTEs






* New submission

* Package suggested but not available for checking: 'RQuantLib'

  The 'RQuantLib' package can be build from sources.
  Inside the function I use the recommended check using `requireNamespace` and 
  if the package isn't available then I produce a warning.

* Missing or unexported object: 'mongolite::gridfs'

  The 'mongolite::gridfs' is currently only available in the development version of mongolite.
  Inside the function I check if proper version of `mongolite` package if available, and
  if 'mongolite` package version is less than '1.5.9' throw an error.



## Reviewer comments

2018-05-20 Uwe Ligges

In addition to the issue reported below,

  Maintainer: RTSVizTeam <rtsvizteam@gmail.com>

is not possible as this is not a single person. See the policies.


# Round 2

## Test environments

* Windows 7, R 3.4.4  (local)

## Submission comments

2018-05-20

Addressed previous comments:  

- updated Maintainer: Irina Kapler <irkapler@gmail.com>

## R CMD check results

0 ERRORs | 0 WARNINGs | 1 NOTE

* New submission


## Reviewer comments

2018-05-21 Swetlana Herbrandt

Thanks, please do not capitalize your Description text.

Please elaborate in your Description text the functionality of your package.

Please replace T and F by TRUE and FALSE.

Your examples are wrapped in \dontrun{}, hence nothing gets tested. Please unwrap the examples if that is feasible and if they can be executed in < 5 sec for each Rd file or create additionally small toy examples. Something like
\examples{
       examples for users and checks:
       executable in < 5 sec
       \dontshow{
              examples for checks:
              executable in < 5 sec together with the examples above
              not shown to users
       }
       donttest{
              further examples for users (not used for checks)
       }
}
would be desirable.

Please ensure that your functions do not write by default or in your examples/vignettes/tests 
in the user's home filespace. That is not allow by CRAN policies. Please only write/save files 
if the user has specified a directory. In your examples/vignettes/tests you can write to tempdir().

# Round 3

## Test environments

* Windows 7, R 3.4.4  (local)

## Submission comments

2018-05-25

Addressed previous comments:  

* updated description to explain the benefits of this package
* replaced T and F by TRUE and FALSE
* added toy example for users and checks
* changed the default location to store historical data to tempdir()


## R CMD check results

0 ERRORs | 0 WARNINGs | 1 NOTE

* New submission


## Reviewer comments

2018-05-29 Uwe Ligges

Actually, not you are not writing to tempdir():

rtsdata/R/storage.r:    
  get.default.option( 'RTSDATA_FOLDER', file.path(dirname(tempdir()), 'data') )
creates the data *not* as a subdirectory of tempdir()

Please rather use
  get.default.option( 'RTSDATA_FOLDER', file.path(tempdir(), 'data') )

# Round 4

## Test environments

* Windows 7, R 3.5.1  (local)

## Submission comments

2018-09-16

Addressed previous comments:  
* changed the default location to store historical data to tempdir()
i.e. get.default.option( 'RTSDATA_FOLDER', tempdir() )


## R CMD check results

0 ERRORs | 0 WARNINGs | 1 NOTE

* New submission
